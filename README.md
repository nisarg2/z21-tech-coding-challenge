# Z21 Tech coding challenge

Coding challenge

## Getting started

-- Best Mutual fund from past returns using node.js

Step 1: Collect historical data from last 7 days using AMFI website. https://portal.amfiindia.com/DownloadNAVHistoryReport_Po.aspx?tp=2&frmdt=01-Apr-2022&todt=11-Apr-2022 .. **This link will provide all schemes NAV from date 1st apr to 11 apr 22 ...Use these query params to change the dates. **
This is a semicolon seperated data.. Data when imported to excel looks like https://docs.google.com/spreadsheets/d/1QTG8ABsHq0YBKJ33RNOe8avtdKVT8JIBGaucfcExw_c/edit?usp=sharing

The important columns fund code, name , NAV and Date are highlighted in yellow

Step 2: Create a GET API to get the top ten fund based on the returns of last 7 days ( Day 7 NAV - Day 1 NAV / Day 7 NAV). Output should include, Fund code, Fund name, 7 day return, latest NAV

Step 3: Create a GET API to get the top ten fund based on highest standard deviation. Use standard deviation on the 7 sample values. Output should include, Fund code, Fund name, standar dev, latest NAV

Step 4: Show this data on Frontend using ReactJS. Have buttons for Top ten funds. Think of best UI and make it.

Note: The data should be trailing based on the last 7 days from current date

## Assesment criteria

-- Code quality
-- Speed of execution
-- Code readability



